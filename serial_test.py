import sys
import time as t
import ARM_ROBOT as rb
num_servo=6

# Start position
pos_init=[0,90,85,90,90,80,45]
pos_balayage_gauche=[0,140,90,20,130,80,45]
pos_balayage_droite=[0,40,90,20,130,80,45]
pos_in=[0,90,160,160,130,20,45]
pos_in3=[0,90,120,160,90,10,45]
pos_in2=[0,90,180,140,90,150,45]

rb.serial_connect()

for i in range(1,7):
    rb.send_create_frame(i,pos_init[i],300,0.4)

for i in range(1,7):
    rb.send_create_frame(i,pos_in[i],300,0.4)

for i in range(1,7):
    rb.send_create_frame(i,pos_in2[i],300,0.4)

for i in range(1,7):
    rb.send_create_frame(i,pos_in[i],300,0.4)

for i in range(1,7):
    rb.send_create_frame(i,pos_in3[i],300,0.4)

for i in range(1,7):
    rb.send_create_frame(i,pos_balayage_gauche[i],300,0.4)

for i in range(1,7):
    rb.send_create_frame(i,pos_init[i],500,0.4)

rb.send_create_frame(5,150,300,0.4)
rb.send_create_frame(2,120,500,0.4)
rb.send_create_frame(3,50,500,0.4)
rb.send_create_frame(5,30,300,0.4)
rb.send_create_frame(2,90,500,0.4)

for i in range(1,7):
    rb.send_create_frame(i,pos_balayage_droite[i],500,0.4)

for i in range(1,7):
    rb.send_create_frame(i,pos_init[i],500,0.4)
t.sleep(2)


rb.close()             # close port