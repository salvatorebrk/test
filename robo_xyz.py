import curses
import ARM_ROBOT as rb
screen = curses.initscr()
curses.cbreak()
screen.keypad(1)

screen.addstr("Enter q to quit ")
screen.refresh()

pos_init=[0,90,85,90,90,80,45]
key = ''
rb.serial_connect()

for i in range(1,7):
    rb.send_create_frame(i,pos_init[i],500,0.4)

while key != ord('q'):
    key = screen.getch()
    if key=='a':
        pos_init[1]=pos_init[1]+10
    elif key=='z':
        pos_init[1]=pos_init[1]-10
    for i in range(1,7):
        rb.send_create_frame(i,pos_init[i],500,0.1)
    screen.refresh()


curses.endwin()
rb.close()