#Python Robot class

import sys
import serial
import time as t
import logging
logging.basicConfig(filename='mylog.log',level=logging.DEBUG,format='%(asctime)s %(message)s')
fd=0
ser=0

servo_reference=[0,1,2,23,4,22,21]

def serial_connect():
    global ser,fd
    try:
        ser = serial.Serial('/dev/serial0',baudrate=115200,timeout=1)  # open serial port
        fd = open("move.csv","w")
    except:
        print("Impossible to open")

    return 0

def close():
    global ser,fd
    fd.close()
    ser.close()
    return 0

def send_create_frame(servo,position,speed,time):
    global ser,servo_reference,fd
    servo_motor=servo_reference[servo]
    pos=int(position * 11.11 + 500);
    if (pos <=2500):
        ser.write("#{}P{}S{}\r\n".format(servo_motor,pos,speed).encode('UTF-8'))
        t.sleep(time)
        logging.info("#{}P{}S{}".format(servo_motor,pos,speed))
        logging.debug("{};{};{};{}\n".format(servo,position,speed,time))
        fd.write("{};{};{};{}\n".format(servo,position,speed,time))
    else:
        print ("Not valid position")

def send_create_frame_no_reg(servo,position,speed,time):
    global ser,servo_reference,fd
    servo_motor=servo_reference[servo]
    pos=int(position * 11.11 + 500);
    if (pos <=2500):
        ser.write("#{}P{}S{}\r\n".format(servo_motor,pos,speed).encode('UTF-8'))
        t.sleep(time)